

// [section] While Loop


// A while loop takes in an expression/ condition.


// Expressions are any unit of code that can be evaluated to a value.
// If the condition evaluates to be truem the statements inside the code block will be executed.
// a loop will iterate a certain number of times until an expression/condition is met.
// Iteration is the term given to the repetition of statements.


/*

    Syntax:
        while(expression/condition){
            statements;
            iteration;

        }


*/


let count = 5;

while(count!==0){
console.log("While: " + count );

count--;

console.log("Value afte iteration: " + count);
}



// [section] do while loop
/*
-a do-while loop works a lot like the while loop, but unlike while loops, do-while loops guarantee that the code will be
executed at least once.

dp{
    statement;
    iteration;
}while(expression/condition)

*/

// let number = Number(prompt("Give me a number."));



// do{
//     console.log("Do while : "+ number);
//     number++;

// }while(number<10);

// let count2 =0;
// while(count2!==0){
//     console.log("While Looping");
// }


// [section] for loop

// - A for loop is more flexible than while and do-while.
// It consist of 3 parts:
// 1. The "initialization" value that will track the progression of the loop.
// 2. The "expression/condition" that will be evaluated which will determine whether the loop will run one more time.
// 3. The "finalExpression" indicates how to advance the loop.

// Syntax:
/* for (initialization; expression/condition; finalExpression){

}

We will create a loop that will start from 0 and end at 20.
Every iteration of the loop, the value of count will be checked if it is equal or less
than 20.

-If the value of count is less than or equal to 20 the statement inside of the loop will execute.
-The value of count will be incremented by one for each iteration.
*/

for(let count = 0; count <=20; count++){
    console.log("The current value of count is: "+count);
}

let myString = "alex";
    // Characters in strings may be counted using the .length property
    // Strings are special compare to other data types, that it has access to functions and other pieces of information.
    // 

    console.log(myString.length);

    // Accessing characters of a string.
    let arr = "";
    for (let i=0; i<myString.length; i++){
        console.log(myString[i]);
        arr = arr + myString[i]+",";
        
    }

    console.log(arr);
    console.log(myString.split());

    let numA =15;
    for(let i=0; numA <=10000; i++){
        numA = numA**2; 
        console.log(numA);
    }

    // Create a string named "myName" with value of "Alex"
    let myName = "Alexandra";

    /*
    Create a loop that will print out the letters of the name individually and print out the number 3 instead
    when the letter to be printed out is a vowel.

    
    */

    for (let i =0; i<myName.length; i++){
        
        switch(myName[i].toLowerCase()){
            case 'a':
                console.log('a');
                break;
            case 'e':
                console.log('e');
                break;
            case 'i':
            console.log('i');
            break;
            case 'o':
                console.log('o');
                break;
            case 'u':
                console.log('u');
                break;
        }
    }

    // [section] Continue and break statements
        // The continue statements allows the code to go to the next iteration
        // of the loop without finishing the execution of all statements in a code block.
        // The break statement is used to terminate the current loop once a match has been found.


        // Creates a loop that if the count value is divided by 2 and the remainder is 0, it will print the number and continue to the next iteration of the loop
        // - How this For Loop works:
        //     1. The loop will start at 0 for the the value of "count".
        //     2. It will che
        //     4. If the expck if "count" is less than the or equal to 20.
        //     3. The "if" statement will check if the remainder of the value of "count" divided by 2 is equal to 0 (e.g 0/2).ression/condition of the "if" statement is "true" the loop will continue to the next iteration.
        //     5. If the value of count is not equal to 0, the console will print the value of "count".
        //     6. The second if statement will check if the value of "count" is greater than 10. (e.g. 0)
        //     7. If the expression/condition of the second "if" statement is false the loop will proceed to the next iteration.
        //     8. The value of "count" will be incremented by 1 (e.g. count = 1)
        //     9. Then the loop will repeat steps 2 to 8 until the expression/condition of the loop is "false" or the condition of the second "if" statemen

        for (let count =0; count<=20; count++){
            
            if(count%2===0){
                // tells the continue the next iteration of the loop.
                // This ignores all statements located after the continue statement.
                continue;
            }
            else if (count>10){
                // Tells the code to terminate/stop the loop even if the expression/condition
                // of the loop defines that it should execute so long as the of count is less than or equal
                // to 20
                // number values after 10 will no longer be printed.

                break;
            }
        
        console.log("continue and break: " + count);

    }


let name ="alexandro";

for(let i = 0; i<name.length; i++){
    console.log(name[i]);
    if(name[i].toLowerCase()==="a"){
        console.log("Continue to next iteration.");
        continue;
    }

    if(name[i].toLowerCase()==="d"){
        console.log("console log before the break.");
        break;
    }

}